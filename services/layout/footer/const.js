const navigations = [
  {
    key: 'our_services',
    title: 'покупателям',
    links: [
      {
        key: 'web',
        title: 'Каталог',
        path: '#',
      },
      {
        key: 'web',
        title: 'Акции',
        path: '#',
      },
      {
        key: 'web',
        title: 'Бренды',
        path: '#',
      },
    ],
  },
  {
    key: 'our_services',
    title: 'О нас',
    links: [
      {
        key: 'web',
        title: 'О компании',
        path: '#',
      },
      {
        key: 'web',
        title: 'Новости',
        path: '#',
      },
      {
        key: 'web',
        title: 'Команда',
        path: '#',
      },
    ],
  },
]

export {navigations};

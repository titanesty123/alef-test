import {navigations} from './const';

export default class FooterApiService {
  static getNavigations() {
    return navigations;
  }
}

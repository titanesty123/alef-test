import Vue from 'vue'

Vue.directive('scroll', {
  inserted: function (el, binding) {
    const onScrollCallback = binding.value
    window.addEventListener('scroll', () => onScrollCallback())
  }
})
